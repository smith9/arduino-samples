// Sweep
// by BARRAGAN <http://barraganstudio.com> 
// This example code is in the public domain.


#include <Servo.h> 
Servo myservo;  // create servo object to control a servo 
Servo leftServo;
Servo rightServo;
                
 
int pos = 0;    // variable to store the servo position 
int align = 50;
int delayMilli = 15;
int START_POS = 50;
int END_POS = 180-START_POS;
boolean doneOnce = false;
void setup() 
{ 
    leftServo.attach(9);  // attaches the servo on pin 9 to the servo object 
    rightServo.attach(10);  // attaches the servo on pin 9 to the servo object 
} 
 
 
void loop() 
{
//  setPosition(0);   
//  setPosition(45);   
//  setPosition(90);   
//  setPosition(135);   
//  setPosition(180);   
//  setPosition(225);   
//  setPosition(270);   
//  setPosition(315);   
//  setPosition(360);   
//  if(!doneOnce) {
//    doneOnce = true;
//    leftServo.write(START_POS);              // tell servo to go to position in variable 'pos' 
//    rightServo.write(END_POS);              // tell servo to go to position in variable 'pos' 
//  }
  sweep();
} 

void setPosition(int position) {
   leftServo.write(position);              // tell servo to go to position in variable 'pos' 
    rightServo.write(position);              // tell servo to go to position in variable 'pos' 
    delay(1000);
}
void sweep() {
  
    for(pos = START_POS; pos <= END_POS; pos += 1)  // goes from 0 degrees to 180 degrees 
  {                                  // in steps of 1 degree 
    leftServo.write(END_POS-pos+align);              // tell servo to go to position in variable 'pos' 
    rightServo.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(delayMilli);                       // waits 15ms for the servo to reach the position 
  } 
  for(pos = END_POS; pos>=START_POS; pos-=1)     // goes from 180 degrees to 0 degrees 
  {                                
    leftServo.write(180-pos);              // tell servo to go to position in variable 'pos' 
    rightServo.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(delayMilli);                       // waits 15ms for the servo to reach the position 
  } 
  
}

void setPositionStart() {
    myservo.write(0);              // tell servo to go to position in variable 'pos' 
  
}
void setPositionEnd() {
    myservo.write(-90);              // tell servo to go to position in variable 'pos' 
  
}
void randomPosition() {
    myservo.write(random(180));              // tell servo to go to position in variable 'pos' 
    delay(1000);                       // waits 15ms for the servo to reach the position  
}
void originalLoop() {
    for(pos = 0; pos < 180; pos += 1)  // goes from 0 degrees to 180 degrees 
  {                                  // in steps of 1 degree 
    myservo.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(15);                       // waits 15ms for the servo to reach the position 
  } 
  for(pos = 180; pos>=1; pos-=1)     // goes from 180 degrees to 0 degrees 
  {                                
    myservo.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(15);                       // waits 15ms for the servo to reach the position 
  } 
}

void testPositions1() {
    myservo.write(0);              // tell servo to go to position in variable 'pos' 
    delay(5000);                       // waits 15ms for the servo to reach the position 
    myservo.write(180);              // tell servo to go to position in variable 'pos' 
    delay(5000);                       // waits 15ms for the servo to reach the position 
}


