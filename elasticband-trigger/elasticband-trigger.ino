/*
  LiquidCrystal Library - Hello World
 
 Demonstrates the use a 16x2 LCD display.  The LiquidCrystal
 library works with all LCD displays that are compatible with the 
 Hitachi HD44780 driver. There are many of them out there, and you
 can usually tell them by the 16-pin interface.
 
 This sketch prints "Hello World!" to the LCD
 and shows the time.
 
  The circuit:
 * LCD RS pin to digital pin 12
 * LCD Enable pin to digital pin 11
 * LCD D4 pin to digital pin 5
 * LCD D5 pin to digital pin 4
 * LCD D6 pin to digital pin 3
 * LCD D7 pin to digital pin 2
 * LCD R/W pin to ground
 * 10K resistor:
 * ends to +5V and ground
 * wiper to LCD VO pin (pin 3)
 
 Library originally added 18 Apr 2008
 by David A. Mellis
 library modified 5 Jul 2009
 by Limor Fried (http://www.ladyada.net)
 example added 9 Jul 2009
 by Tom Igoe
 modified 22 Nov 2010
 by Tom Igoe
 
 This example code is in the public domain.

 http://www.arduino.cc/en/Tutorial/LiquidCrystal
 */
 
#include <LiquidCrystal.h>
#include <Servo.h> 

#define echoPin 7
#define trigPin 8
#define SCREEN_WIDTH 16

float distance;
long duration;
char line1[17] = "";
char line2[17] = "";
Servo cogServo;  // create servo object to control a servo 
                // a maximum of eight servo objects can be created 
Servo armServo;                

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 3, 4, 5, 6);

// VSS - Gnd
// VDD - 5v
// V0 - Resistor - Gnd
// RS - D12
// RW - Gnd
// E - D11

// D4 - D3
// D5 - D4
// D6 - D5
// D7 - D6

// A - 5v
// K - Gnd

void setup() {
  cogServo.attach(9);  // attaches the servo on pin 9 to the servo object 
  armServo.attach(10);  // attaches the servo on pin 9 to the servo object 
  moveServo(cogServo, "cog", 180);
  moveServo(armServo, "arm", 180);

  // set up the LCD's number of columns and rows: 
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("hello, world!");
  
  // setup serial for sonar
  Serial.begin(9600);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
}

void loop() {
  // print the number of seconds since reset:
//  writeToScreen("David", 1, 0);
  
    digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  // distance in m
  distance = duration / 5820.0;
//  if(distance > 2) {
    sprintf(line1, "%s", "hi-------");
  Serial.println(line1);
  if(distance > 2) {
    strncpy(line1, "Where are you?", SCREEN_WIDTH);
    moveServo(armServo, "arm", 0);

  } else if(distance > 1.6) {
    strncpy(line1, "Come closer", SCREEN_WIDTH);
    moveServo(armServo, "arm", 45);
  } else if(distance > 1.0) {
    strncpy(line1, "Come even closer", SCREEN_WIDTH);
    moveServo(armServo, "arm", 90);
  } else if(distance > 0.5) {
    strncpy(line1, "Still too far", SCREEN_WIDTH);
    moveServo(armServo, "arm", 135);
  } else {
    strncpy(line1, "You did it!!!!!", SCREEN_WIDTH);
    moveServo(armServo, "arm", 180);
//    delay(2000);
  }
  lcd.clear();
//  writeToScreen("                ", 0, 0);
  writeToScreen(line1, 0, 0);
  Serial.println(distance);
  char distanceBuff[5];
  dtostrf(distance, 4, 2, distanceBuff);
  
  writeToScreen(distanceBuff, 1, 0);
//  delay(300);
  fire(distance);
  
}

void fire(float distance) {
  if(distance <= 0.5) {
    
    moveServo(cogServo, "cog", 0);
    delay(1000);
    moveServo(cogServo, "cog", 180);
  }
}

void moveServo(Servo servo, String description, int position) {
  servo.write(position);
  Serial.println("Moving " + description + " to " + position);
}

void writeToScreen(String message, int row, int column) {
  Serial.println("message: " + message + " at row: " + row + " column: " + column);
  lcd.setCursor(column, row);
  lcd.print(message);  
}


